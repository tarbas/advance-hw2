$('document').ready(function () {

    // бургер


    $('.fa-bars').click(function () {
        $('.fa-bars').hide();
        $('.dropdown-menu').slideDown(500);
        $('.fa-times').show();
    });

    $('.fa-times').click(function () {
        $('.fa-times').hide();
        $('.dropdown-menu').slideUp(500);
        $('.fa-bars').show();
    });

    $('.nav-link').click(function () {
        $('.fa-times').hide();
        $('.dropdown-menu').slideUp(500);
        $('.fa-bars').show();
    });

    $(window).resize(function () {
        if ($('body').outerWidth() > 980) {
            $('.fa-times').hide();
            $('.dropdown-menu').hide();
            $('.fa-bars').show();
        } else return;
    });

});
